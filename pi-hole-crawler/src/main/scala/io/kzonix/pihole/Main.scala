package io.kzonix.pihole
import cats.effect.{ ExitCode, IO, IOApp }
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.client.dsl.io._
import org.http4s.headers._
import org.http4s.{ AuthScheme, Credentials, MediaType, Method, Uri }

import scala.concurrent.ExecutionContext.Implicits.global
object Main extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    val value = BlazeClientBuilder[IO](global).resource.use { client =>
      val request = Method.GET(
        Uri.uri("https://example.com/"),
        Authorization(Credentials.Token(AuthScheme.Bearer, "open sesame")),
        Accept(MediaType.application.json)
      )

      client.fetch[String](request)(resp => {
        IO.apply(
          resp.body
            .buffer(1)
            .map(b => { if (new String(Array[Byte](b)).trim.isEmpty) "" else new String(Array[Byte](b)) })
            .compile
            .toList
            .unsafeRunSync()
            .mkString
        )
      })
    }
    val str = value.unsafeRunSync()
    println(str)
    IO(ExitCode.Success)
  }

}
