name := "sturdy-octo-broccoli"
version := "0.1"
scalaVersion := "2.12.8"

lazy val root = (project in file("."))
lazy val pihole = (project in file("pi-hole-crawler"))
  .settings(
    organization := "io.kzonix",
    name := "pi-hole-crawler",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.12.8",
    libraryDependencies ++= Seq(
      "org.http4s"     %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s"     %% "http4s-circe"        % Http4sVersion,
      "org.http4s"     %% "http4s-dsl"          % Http4sVersion,
      "org.http4s"     %% "http4s-blaze-client" % Http4sVersion,
      "org.specs2"     %% "specs2-core"         % Specs2Version % "test",
      "ch.qos.logback" % "logback-classic"      % LogbackVersion
    ),
    addCompilerPlugin("org.spire-math" %% "kind-projector"     % "0.9.6"),
    addCompilerPlugin("com.olegpy"     %% "better-monadic-for" % "0.2.4")
  )
val Http4sVersion  = "0.20.0-M6"
val Specs2Version  = "4.1.0"
val LogbackVersion = "1.2.3"

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding",
  "UTF-8",
  "-language:higherKinds",
  "-language:postfixOps",
  "-feature",
  "-Ypartial-unification",
  "-Xfatal-warnings",
)
